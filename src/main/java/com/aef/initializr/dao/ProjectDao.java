package com.aef.initializr.dao;

import com.aef.initializr.model.Project;
import com.aef3.data.impl.AbstractDAOImpl;
import org.springframework.stereotype.Repository;


/* Generated By Nicico System Generator ( Powered by Dr.Adldoost :D ) */

@Repository
public class ProjectDao extends AbstractDAOImpl<Project, Long> {

    public ProjectDao() {
        super(Project.class);
    }
}
